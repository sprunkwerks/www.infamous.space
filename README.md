# [Infamous](http://www.infamous.space)
Infamous is a Gaming community and EVE Online Alliance. This is the main landing page for infamous, and is built on the Skeleton Boilerplate.

Check out <http://getskeleton.com> for documentation and details on Skeleton.

```
www/
├── index.html
├── css/
│   ├── normalize.min.css
│   └── skeleton.css
└── images/
    └── favicon.ico

```

## Acknowledgement

Skeleton was created by [Dave Gamache](https://twitter.com/dhg) for a better web.